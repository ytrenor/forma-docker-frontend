FROM            nginx:1.15
MAINTAINER      Brice Argenson <brice@clevertoday.com>

COPY            nginx.conf    /etc/nginx/nginx.conf
COPY            html          /usr/share/nginx/html
